/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.sseclient;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.Random;


/**
 * Created by hansolo on 27.11.15.
 */
public class Main {
    private static final Random RND = new Random();

    public static void main(String[] args) {
        SseClient.INSTANCE.subscribe("sensor/+", 0);
        SseClient.INSTANCE.setOnSseEventReceived(event -> {
            JSONObject jsonObj = (JSONObject) JSONValue.parse(event.MESSAGE.toString());
            System.out.println(jsonObj.get("topic")  + " : " + jsonObj.get("message"));
        });
        int counter = 0;
        while(true) {
            try {
                Thread.sleep(100);
                counter++;
                if (counter == 50) {
                    double temperature        = RND.nextDouble() * (25 - 10) + 10;
                    double pressure           = RND.nextDouble() * (1030 - 940) + 940;
                    double humidity           = RND.nextDouble() * (99 - 40) + 40;
                    SseClient.INSTANCE.publish("sensor/JavaClient", "{ \"temperature\":" + temperature +
                                                                     ",\"pressure\":" + pressure +
                                                                     ", \"humidity\":" + humidity + " }", 0, false);
                    counter = 0;
                }
            } catch(InterruptedException e) {}
        }
    }
}
