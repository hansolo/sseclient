/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.sseclient;

import org.glassfish.jersey.media.sse.EventListener;
import org.glassfish.jersey.media.sse.EventSource;
import org.glassfish.jersey.media.sse.SseFeature;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Created by hansolo on 27.11.15.
 */
public enum SseClient {
    INSTANCE;

    private static final String                    REST_URL       = "http://localhost:8080/";
    private Map<String, EventSource>               eventSourceMap = new HashMap<>(16);
    private CopyOnWriteArrayList<SseEventListener> listenerList   = new CopyOnWriteArrayList<>();


    // ******************** Methods *******************************************
    public void publish(final String TOPIC, final String MESSAGE, final int QOS, final boolean RETAINED) {
        try {
            URL url = new URL(REST_URL + "publish");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("topic", TOPIC);
            jsonObj.put("msg", MESSAGE);
            jsonObj.put("qos", QOS);
            jsonObj.put("retained", RETAINED);

            OutputStream os = conn.getOutputStream();
            os.write(jsonObj.toJSONString().getBytes("UTF-8"));
            os.flush();

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())))) {
                    String output;
                    while ((output = br.readLine()) != null) {
                        //System.out.println(output);
                    }
                }
                conn.disconnect();
            } else {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void subscribe(final String TOPIC, final int QOS) {
        if (null == TOPIC) return;
        try {
            if (eventSourceMap.containsKey(TOPIC)) return;

            Client        client      = ClientBuilder.newBuilder().register(SseFeature.class).build();
            WebTarget     target      = client.target(REST_URL + "subscribe?topic=" + URLEncoder.encode(TOPIC, "UTF-8") + "&qos=" + clamp(0, 2, QOS));
            EventSource   eventSource = EventSource.target(target).build();
            EventListener listener    = inboundEvent -> {
                JSONObject jsonObj = (JSONObject) JSONValue.parse(inboundEvent.readData(String.class, MediaType.APPLICATION_JSON_TYPE));
                fireSseEvent(new SseEvent(SseClient.this, TOPIC, jsonObj));
            };
            eventSource.register(listener);
            eventSource.open();
            eventSourceMap.put(TOPIC, eventSource);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
    public void unSubscribe(final String TOPIC) {
        if (eventSourceMap.containsKey(TOPIC)) {
            eventSourceMap.get(TOPIC).close();
            eventSourceMap.remove(TOPIC);
            System.out.println("Unsubscribed from " + TOPIC);
        }
    }

    private int clamp(final int MIN, final int MAX, final int VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }


    // ******************** Event handling ************************************
    public final void setOnSseEventReceived(final SseEventListener LISTENER) { addSseEventListener(LISTENER); }
    public final void addSseEventListener(final SseEventListener LISTENER) { listenerList.add(LISTENER); }
    public final void removeSseEventListener(final SseEventListener LISTENER) { listenerList.remove(LISTENER); }

    public void fireSseEvent(final SseEvent EVENT) { listenerList.forEach(listener -> listener.onSseEvent(EVENT)); }


    // ******************** Inner Classes *************************************
    public class SseEvent extends EventObject {
        public final String     TOPIC;
        public final JSONObject MESSAGE;

        // ******************** Constructor ***********************************
        public SseEvent(Object source, final String TOPIC, final JSONObject MESSAGE) {
            super(source);
            this.TOPIC   = TOPIC;
            this.MESSAGE = MESSAGE;
        }
    }

    public interface SseEventListener extends java.util.EventListener {
        void onSseEvent(SseEvent event);
    }
}
